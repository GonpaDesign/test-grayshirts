DESAFIO GRAYSHIRTS
=======

## Tecnologias

Programa realizado en Java 8, utilizando el framework Spring Boot 2.2.1 en back-end.

Como base de datos se utilizo MongoDB (tipo noSQL).

Implementacion realizada a traves del PAAS Heroku en un Dyno gratuito. El repositorio subido se copio a GitHub por la implementacion directa.

## Descripción del desafio

a idea es crear una aplicación que tenga dos endpoints, uno recibe un POST con un JSON en el body y guarda la entidad en la base de datos. El segundo recibe un GET con el ID y renderiza un JSON mostrando el objeto

Un ejemplo del curl para crear documentos es el siguiente:

curl -X POST "https://nombredetuapp.herokuapp.com/api/websites" -H 'token: 123456789' -H 'content-type: application/json' -d '{domain: "www.planes.com.ar", ownerId: 123456789, leadCount: 0, plan: "GOLD", labels: ["PRIVATE", "UPGRADED"]}'

Respuesta esperada:

Status: 201

Body: {id: 1, domain: "www.planes.com.ar", ownerId: 123456789, plan: "GOLD", labels: ["PRIVATE", "UPGRADED"]}

Un ejemplo del curl para obtener documentos es el siguiente:

curl "https://nombredetuapp.herokuapp.com/api/websites/1" -H 'token: 123456789' 

Respuesta esperada:

Status: 200

Body: {id: 1, domain: "www.planes.com.ar", ownerId: 123456789, leadCount: 0, plan: "GOLD", labels: ["PRIVATE", "UPGRADED"]}

Con respecto al token, cualquier token que no sea 123456789 tiene que ser rechazado con el status code correspondiente.